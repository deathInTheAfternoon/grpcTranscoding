Running directly in Windows:
Need the protoc compiler to convert idl to stub: https://github.com/protocolbuffers/protobuf/releases

From here you can download the Windows release and put the binary on your PATH. I put protoc.exe in the golang installation folder 'C:/Go/bin'

You also need protoc-gen-go which creates the pb.go language stubs.
	go get -u github.com/golang/protobuf/protoc-gen-go
This installs in $GOPATH/bin so, for ease of use, you can put that on your PATH e.g. export PATH=$PATH:$GOPATH/bin

You will need the golang grpc libs (see https://grpc.io/docs/quickstart/go/):
go get -u google.golang.org/grpc

You also need the two .proto files that enable annotations.
Under your 'grpcContracts' folder
mkdir -p googleapis/google/api  
curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/annotations.proto > googleapis/google/api/annotations.proto 
curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/http.proto > googleapis/google/api/http.proto

* Prereq: Mage
* Added reflection to server
* Added name resolver to client
* Installed nodejs v12.10 (because of npm) on WSL (see https://github.com/nodesource/distributions/blob/master/README.md#debinstall)
* Install grpc-web plugin for protoc (download binary from https://github.com/grpc/grpc-web/releases/ - and 'chmod +x' then put on PATH - e.g. ~/protoc-code/bin)
* Rename downloaded protoc-gen-grpc-web-1.0.6-linux-x86_64, to protoc-gen-grpc-web so the plugin can find it.
* Assumes nginx installed at '/c/work/dev/gel/go_workspace/bin/nginx/'. Note: mage uses WSL paths - not Windows paths. So this command wont work on Win.
* By default, client.js is assuming nginx listening on port 80 and can proxy /grpc-web-api to Envoy.

To compile grpc-web-app you need to install nodejs (to get webpack). The run 'mage installnodemodules' which will install JS dependencies in grpc-web-app/node_modules. Then run 'mage buildwebpackbundle' to generate main.js under grpc-web-app/dist.

For grpc-web example, start echoServer, envoy proxy, then load Windows path to index.html in browser (with consle/F12 open to see grpc-web response).

When all components are running (Nginx, Enovy, echoServer), hitting 'localhost' in browser will load static page via nginx which then calls back to nginx -> envoy -> echoServer.