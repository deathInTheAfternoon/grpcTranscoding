# Prereq: install protoc using: go get -u github.com/golang/protobuf/protoc-gen-go
# Prereq: and ensure 'protoc' on PATH

# Prereq: download proto files required to enable REST annotations within .proto files. 
# You can also git clone https://github.com/googleapis/googleapis, but this is 10s of MB.
mkdir -p googleapis/google/api  
curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/annotations.proto > googleapis/google/api/annotations.proto 
curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/http.proto > googleapis/google/api/http.proto

