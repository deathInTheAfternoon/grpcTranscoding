module gitlab.com/deathInTheAfternoon/grpcTranscoding

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/magefile/mage v1.8.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/piglatin v0.0.0-20140311054444-ab61287b9936 // indirect
	google.golang.org/genproto v0.0.0-20190819201941-24fa4b261c55
	google.golang.org/grpc v1.23.0
)
