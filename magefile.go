// +build mage

// The following targets can be used to compile idl proto files and then compile client and server programs.
// Use -v option to see console logs.
package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Compile and install all cmds.
// This will compile the proto idl files then build both the client and server cmds.
func BuildAll() error {
	// redirect logging to stdout
	//log.SetOutput(os.Stdout)

	mg.SerialDeps(ProtoGolang, ProtoBinary, BuildServer, BuildClient)
	log.Println("BuildAll finished.")
	return nil
}

// Compile and install the gprc server.
// Only compiles the server golang code. Does not compile proto idl (see proto~ flags).
func BuildServer() error {
	//mg.Deps(InstallDeps)
	log.Println("Installing Server...")
	err := sh.Run("go", "install", "gitlab.com/deathInTheAfternoon/grpcTranscoding/cmd/echoServer") //installCmd.Run()
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// Compile and install the grpc client.
// Only compiles the client golang code. Does not compile proto idl (see proto~ flags).
func BuildClient() error {
	log.Println("Installing client...")
	err := sh.Run("go", "install", "gitlab.com/deathInTheAfternoon/grpcTranscoding/cmd/echoClient")
	if err != nil {
		return err
	}
	return nil
}


// Compile all proto idl.
func Protos() error {
	mg.Deps(ProtoGolang, ProtoBinary)
	return nil
}

// Compile idl and produce only golang stubs.
func ProtoGolang() error {
	log.Println("Compiling protos to golang...")
	err := sh.Run("protoc", "-I", "./api/grpcContracts/echoContract",
		"-I", "./api/grpcContracts/googleapis",
		"--go_out=plugins=grpc:./api/grpcStubs/echoContract",
		"echoContract.proto")
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

// Compile idl and produce only binary stubs.
func ProtoBinary() error {
	log.Println("Compiling protos to binary...")
	err := sh.Run("protoc", "-I", "./api/grpcContracts/echoContract",
		"-I", "./api/grpcContracts/googleapis",
		"--include_imports",
		"--include_source_info",
		"--descriptor_set_out", "./api/grpcStubs/echoContract/echoContract.pb",
		"echoContract.proto")
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

// Compile idl and produce javascript stubs. This is used for grpc-web (which supports only unary & server-streaming).
// 'js_out' generates the payload classes and grpc-web_out generates the stub class. 'commonjs' puts them in a single file.
func ProtoJS() error {
	log.Println("Compiling protos to JS...")
	err := sh.Run("protoc", "-I", "./api/grpcContracts/echoContract",
	"--js_out=import_style=commonjs:./api/grpcStubs/echoContract",
	"--grpc-web_out=import_style=commonjs,mode=grpcwebtext:./api/grpcStubs/echoContract",
	"echoContractWeb.proto")
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

// Under WSL, you may see an error like 'Error: EACCES: permission denied, rename'. This is a bug with VSCode. Close it and run mage from WSL terminal. 
// Once installed you can restart VSCode.
func InstallNodeModules() error {
	log.Println("Installing node modules - will take a few mins...")
	err := sh.Run("npm", "run", "postinstall")
	if err != nil {
		return err
	}
	return nil
}

func BuildWebpackBundle() error {
	log.Println("Compiling client.js...")
	err := sh.Run("npm", "run", "generateWebpack")
	if err != nil {
		return err
	}
	return nil
}

// Note: WSL path used below...if you're running on Windows it will need 'c:/work/...'.
func CopyToNginx() error {
	log.Println("Copy config and static pages to Nginx...")
	err := sh.Run("cp", "./grpc-web-app/index.html", "/c/work/dev/gel/go_workspace/bin/nginx/html/")
	if err != nil {
		return err
	}
	err = sh.Run("cp", "-r", "./grpc-web-app/dist", "/c/work/dev/gel/go_workspace/bin/nginx/html/")
	if err != nil {
		return err
	}
	err = sh.Run("cp", "-r", "./grpc-web-app/nginx.conf", "/c/work/dev/gel/go_workspace/bin/nginx/conf/")
	if err != nil {
		return err
	}
	return nil
}

// A custom install step if you need your bin someplace other than go/bin
func Install() error {
	mg.Deps(BuildAll)
	fmt.Println("Installing...")
	return os.Rename("./MyApp", "/usr/bin/MyApp")
}

// Manage your deps, or running package managers.
func InstallDeps() error {
	fmt.Println("Installing Deps...")
	cmd := exec.Command("go", "get", "github.com/stretchr/piglatin")
	return cmd.Run()
}

