package main

// 'go get github.com/sirupsen/logrus installs under $GOPATH/src folder.
import (
	"fmt"
	"io"
	"net"

	echoContract "gitlab.com/deathInTheAfternoon/grpcTranscoding/api/grpcStubs/echoContract"

	"context"

	log "github.com/sirupsen/logrus"
	grpc "google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	connProtocol = "tcp"
	connPort     = ":51000"
)

// Implement the idl intf...
type echoServerImpl struct{}

func (s *echoServerImpl) EchoThis(ctx context.Context, msg *echoContract.MessageToEcho) (*echoContract.EchoResponse, error) {
	log.Printf("Echoing %v", msg.GetMsg())
	return &echoContract.EchoResponse{Response: "Hi " + msg.GetMsg()}, nil
}

func (s *echoServerImpl) BidiEchoThis(stream echoContract.Echo_BidiEchoThisServer) error {
	for {
		msg, err := stream.Recv()
		if err != nil {
			log.Printf("EchoServer pulled error from stream (%v)", err)
			if err == io.EOF {
				log.Printf("EchoServer received EOF")
				return nil
			}
			return err
		}

		log.Printf("EchoServer received: %v", msg.Msg)
		stream.Send(&echoContract.EchoResponse{Response: fmt.Sprintf("EchoServer echoing: %v", msg.Msg)})
	}
}

func main() {
	log.Infof("Starting echo server on port %s", connPort)

	listener, err := net.Listen(connProtocol, connPort)
	if err != nil {
		log.Fatalf("net.Listen() failed: %v", err)
	}

	grpcServer := grpc.NewServer()
	echoContract.RegisterEchoServer(grpcServer, &echoServerImpl{})
	reflection.Register(grpcServer)
	if err := grpcServer.Serve(listener); err != nil {
		log.Fatalf("Error handling request: %v", err)
	}
}
