package main

import (
	"context"
	"fmt"
	"log"
	"time"

	echoContract "gitlab.com/deathInTheAfternoon/grpcTranscoding/api/grpcStubs/echoContract"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/resolver"
	"google.golang.org/grpc/status"
)

func main() {
	serviceString := getServiceName()

	log.Printf("Dialing %s \n", serviceString)
	conn, err := grpc.Dial(serviceString, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to dial server: %v", err)
	}
	defer conn.Close()

	client := echoContract.NewEchoClient(conn)

	echoRandomStrings(client, 3)
	echoStream(client)

}

func getServiceName() string {
	return fmt.Sprintf("%s:///%s", echoServerScheme, echoServerServiceName)
}

func echoRandomStrings(client echoContract.EchoClient, n int) {
	for i := 0; i < n; i++ {
		callEchoThis(client, fmt.Sprintf("Robot %d", i))
	}
}

func callEchoThis(client echoContract.EchoClient, echoMessage string) {

	// We timeout afer 1 second (time.Second = 1s)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := client.EchoThis(ctx, &echoContract.MessageToEcho{Msg: echoMessage})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Server responded: %s", r.GetResponse())
}

//////// Bidi

// Pull message from stream
func handleMsg(stream echoContract.Echo_BidiEchoThisClient) error {
	res, err := stream.Recv()
	if status.Code(err) != codes.OK {
		log.Fatalf("stream.Recv() = %v, %v; want _, status.Code(err)=%v", res, err, codes.OK)
		return err
	}
	if err != nil {
		log.Fatalf("stream.Recv() returned expected error %v\n", err)
		return err
	}
	log.Printf("EchoClient received message: %q\n", res.GetResponse())
	return nil
}

// Push message onto stream
func echoStream(client echoContract.EchoClient) {

	// We timeout afer 1 second (time.Second = 1s)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	stream, err := client.BidiEchoThis(ctx)
	if err != nil {
		log.Fatalf("EchoClien failed to create stream: %v", err)
	}

	if err := stream.Send(&echoContract.MessageToEcho{Msg: "Streaming via HTTP/2!"}); err != nil {
		log.Fatalf("Stream error: %v", err)
	}
	if err := stream.Send(&echoContract.MessageToEcho{Msg: "And again via HTTP/2!"}); err != nil {
		log.Fatalf("Stream error: %v", err)
	}

	handleMsg(stream)
	handleMsg(stream)

	// At this point, deferred cancel() sill send the
	// following on function exit 'code = Canceled desc = context canceled'
}

//////////////////////////////////////////////// Example of the in-built grpc name resolver. This would usually be hosted somewhere central. /////////////////////////////
type exampleResolverBuilder struct{}

const (
	echoServerScheme      = "echoServer"
	echoServerServiceName = "resolver.echoServer.grpc.io"
	registeredAddr        = "localhost:51000"
)

func (*exampleResolverBuilder) Build(target resolver.Target, cc resolver.ClientConn, opts resolver.BuildOption) (resolver.Resolver, error) {
	r := &exampleResolver{
		target: target,
		cc:     cc,
		addrsStore: map[string][]string{
			echoServerServiceName: {registeredAddr},
		},
	}
	r.start()
	return r, nil
}
func (*exampleResolverBuilder) Scheme() string { return echoServerScheme }

// exampleResolver is a
// Resolver(https://godoc.org/google.golang.org/grpc/resolver#Resolver).
type exampleResolver struct {
	target     resolver.Target
	cc         resolver.ClientConn
	addrsStore map[string][]string
}

func (r *exampleResolver) start() {
	addrStrs := r.addrsStore[r.target.Endpoint]
	addrs := make([]resolver.Address, len(addrStrs))
	for i, s := range addrStrs {
		addrs[i] = resolver.Address{Addr: s}
	}
	r.cc.UpdateState(resolver.State{Addresses: addrs})
}
func (*exampleResolver) ResolveNow(o resolver.ResolveNowOption) {}
func (*exampleResolver) Close()                                 {}

func init() {
	// Register the example ResolverBuilder. This is usually done in a package's
	// init() function.
	resolver.Register(&exampleResolverBuilder{})
}
