const {MessageToEcho, EchoResponse} = require('./echoContractWeb_pb.js');
const {EchoClient} = require('./echoContractWeb_grpc_web_pb.js');

var client = new EchoClient('http://' + window.location.hostname + '/grpc-web-api');

var request = new MessageToEcho();
request.setMsg('gRPC-Web World!');

client.echoThis(request, {}, (err, response) => {
  console.log(response.getResponse());
});